//matrix class
//create new empty for given size
//return dimensions
//create new w/ random data for given size
//using ES6 class notation
'use strict';

class Matrix{
  constructor(row,col){
    this.__row=row;
    this.__col=col;
    var matrix=[];
    for(var i=0;i<row;i++){
      matrix[i]=[];
    }
    return matrix;
  }

  //getters and setters
  set row(row){this.__row=row;}
  get row(){return this.__row;}
  set col(col){this.__col=col;}
  get col(){return this.__col;}

  randomize(){
    for(var i=0;i<this.__row;i++){
      for(var j=0;j<this.__col;j++){
        this[i][j]=Math.random().toFixed(0);
      }
    }
  }

  //print matrix to console
  toString(){
    for(var r=0;r<this.__row;r++){
      console.log(this[r]);//should print out row array
    }
  }
}

module.exports=Matrix;
