var Matrix=require('./mtx-class.js');
/*
The Rules

For a space that is 'populated':
Each cell with one or no neighbors dies, as if by solitude.
Each cell with four or more neighbors dies, as if by overpopulation.
Each cell with two or three neighbors survives.
For a space that is 'empty' or 'unpopulated'
Each cell with three neighbors becomes populated.
*/
var TAG = 'game_rend';

var gridSize = 8;
var play = false;

var isDebug = true;
var debugLevel = 3;
//level 0 = always
//level 1 = start()
//level 2 = stepEvolution()
//level 3 = getNeighbors()

var grid = new Matrix(gridSize,gridSize);

function debug(tag,lvl,msg){
  var levels = [0,2,debugLevel];
  if(isDebug==true && (levels.indexOf(lvl)>=0)){
    console.log(tag+': '+msg);
  }
}

//this isn't doing anything
function ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

//on start clicked
function start(){
  console.log(TAG+': start clicked');
  debug(TAG,1,'gridlen='+grid.length);
  debug(TAG,1,'grid[0]len='+grid[0].length);
  var table = document.querySelector('#GameTable');
  if(table.children.length !=0){
    debug(TAG,1,'table child len!=0');
  }else{
    for(var i=0;i<grid.length;i++){
      var tr = document.createElement('tr');
      for(var j=0;j<grid.length;j++){
        var td = document.createElement('td');
        td.className = "dead";
        var ID = i+'-'+j;
        td.id = ID;
        td.setAttribute('onclick','togglePopulation('+i+','+j+',true)');
        tr.appendChild(td);
        grid[i][j] = 0;
        console.log('grid['+i+']['+j+']');
      }
      table.appendChild(tr);
      debug(TAG,1,'row appended---------------');
    }
  }
}

function playEvolution(){
  play = !play;
  while(play){
    setTimeout(()=>{
      stepEvolution();
    },2000);
  }
}

/*
The Rules

For a space that is 'populated':
Each cell with one or no neighbors dies, as if by solitude.
Each cell with four or more neighbors dies, as if by overpopulation.
Each cell with two or three neighbors survives.
For a space that is 'empty' or 'unpopulated'
Each cell with three neighbors becomes populated.
*/
function stepEvolution(){
  var tmpGrid = new Matrix(gridSize,gridSize);
  debug('step',2,'BEGIN');
  for(var x=0;x<gridSize;x++){
    for(var y=0;y<gridSize;y++){
      var nCount = getNeighbors(x,y);
      debug('step',2,'['+x+','+y+']-a='+nCount[0]+'-d='+nCount[1]);
      if(grid[x][y]==1){
        debug('step',2,'t0');
        if(nCount[0]<=1 || nCount[0]>=4){//cell died
          tmpGrid[x][y]=0;
          togglePopulation(x,y,false);
          debug('step',2,'t1');
        }else{//cell lived on
          tmpGrid[x][y]=1;
          debug('step',2,'t2');
        }
      }else{
        debug('step',2,'t3');
        if(nCount[0]==3){
          tmpGrid[x][y]=1;
          togglePopulation(x,y,false);
          debug('step','t4');
        }else{
          tmpGrid[x][y]=0;
        }
      }
    }
  }
  grid=tmpGrid;
  grid.toString();
  debug('step',0,'END');
  //need to redraw now
}

/*
neighbors
  y
x 1   2   3 x
  8   X   4
  7   6   5
  y

  0,0 0,1 0,2
  1,0 1,1 1,2
  2,0 2,1 2,2

*/
function getNeighbors(x,y){
  var dCnt=0,
      aCnt=0;
  debug('getN',3,'start['+x+','+y+']');
  if(x>0 && x<gridSize-1 && y>0 && y<gridSize-1){
    grid[x-1][y-1]==1?aCnt++:dCnt++;//test n1
    grid[x-1][y]==1?aCnt++:dCnt++;//test n2
    grid[x-1][y+1]==1?aCnt++:dCnt++;//test n3
    grid[x][y-1]==1?aCnt++:dCnt++;//test n8
    grid[x][y+1]==1?aCnt++:dCnt++;//test n4
    grid[x+1][y-1]==1?aCnt++:dCnt++;//test n7
    grid[x+1][y]==1?aCnt++:dCnt++;//test n6
    grid[x+1][y+1]==1?aCnt++:dCnt++;//test n5
    debug('getN',3,'t1');
  }else{//calculate corner case
    if(x==0 && y==0){
      grid[x][y+1]==1?aCnt++:dCnt++;//test n4
      grid[x+1][y]==1?aCnt++:dCnt++;//test n6
      grid[x+1][y+1]==1?aCnt++:dCnt++;//test n5
      debug('getN',3,'t2');
    }else if(x==0 && y==gridSize-1){
      grid[x][y-1]==1?aCnt++:dCnt++;//test n8
      grid[x+1][y-1]==1?aCnt++:dCnt++;//test n7
      grid[x+1][y]==1?aCnt++:dCnt++;//test n6
      debug('getN',3,'t3');
    }else if(x==gridSize-1 && y==0){
      grid[x-1][y]==1?aCnt++:dCnt++;//test n2
      grid[x-1][y+1]==1?aCnt++:dCnt++;//test n3
      grid[x][y+1]==1?aCnt++:dCnt++;//test n4
      debug('getN',3,'t4');
    }else if(x==gridSize-1 && y==gridSize-1){
      grid[x-1][y-1]==1?aCnt++:dCnt++;//test n1
      grid[x-1][y]==1?aCnt++:dCnt++;//test n2
      grid[x][y-1]==1?aCnt++:dCnt++;//test n8
      debug('getN',3,'t5');
    }else{
      debug('getNeighbor',3,'err1@['+x+','+y+']');
      //now calculate edge case
      if(x==0){//<<<<<<<<--------------------------------this isn't calculating the edge case for x==0
        grid[x][y-1]==1?aCnt++:dCnt++;//test n8
        grid[x][y+1]==1?aCnt++:dCnt++;//test n4
        grid[x+1][y-1]==1?aCnt++:dCnt++;//test n7
        grid[x+1][y]==1?aCnt++:dCnt++;//test n6
        grid[x+1][y+1]==1?aCnt++:dCnt++;//test n5
        debug('getN',3,'t6');
      }else if(x==gridSize-1){
        grid[x-1][y-1]==1?aCnt++:dCnt++;//test n1
        grid[x-1][y]==1?aCnt++:dCnt++;//test n2
        grid[x-1][y+1]==1?aCnt++:dCnt++;//test n3
        grid[x][y-1]==1?aCnt++:dCnt++;//test n8
        grid[x][y+1]==1?aCnt++:dCnt++;//test n4
        debug('getN',3,'t7');
      }else{
        debug('getN',3,'err2');
      }
    }
  }
  debug('getNeighbor',0,'aCnt:'+aCnt+'-dCnt:'+dCnt);
  return [aCnt,dCnt];
}

function togglePopulation(x,y,set){
  debug(TAG,0,'togglePopulation='+x+y+'-'+set);
  var cell = document.querySelector('#\\3'+x+' -'+y);
  if(cell.className=="dead"){
    cell.className="alive";
    if(set){grid[x][y]=1;}
  }else{
    cell.className="dead";
    if(set){grid[x][y]=0;}
  }
  //getNeighbors(x,y);
}

function reset(){
  for(var x=0;x<gridSize-1;x++){
    for(var y=0;y<gridSize-1;y++){
      var cell = document.querySelector('#\\3'+x+' -'+y);
        cell.className="dead";
        grid[x][y]=0;
    }
  }
}

//generate selector path (for coordinates)
function generatePath(){

}












//
